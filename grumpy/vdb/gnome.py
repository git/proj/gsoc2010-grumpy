# -*- coding: utf-8 -*-
"""
    grumpy.vdb.gnome
    ~~~~~~~~~~~~~~~~

    This module contains version check lookups for GNOME project packages.

    :copyright: (c) by 2010 Priit Laes.
    :license: BSD, see LICENSE for details.
"""
import urllib2

class gnome_checker(object):
    # TODO: Shall we use some kind of yaml or json based format instead?
    # Or move this data into DB?
    pkgs = {
        # Package in Portage        :   Package in GNOME
        ## Platform
        'gnome-base/gconf'          : 'platform:GConf',
        'gnome-base/orbit'          : 'platform:ORBit2',
        'gnome-extra/at-spi'        : 'platform:at-spi',
        'dev-libs/atk'              : 'platform:atk',
        'media-libs/audiofile'      : 'platform:audiofile',
        'media-sound/esound'        : 'platform:esound',
        'dev-libs/glib'             : 'platform:glib',
        'gnome-base/gnome-mime-data': 'platform:gnome-mime-data',
        'gnome-base/gnome-vfs'      : 'platform:gnome-vfs',
        'x11-libs/gtk+'             : 'platform:gtk+',
        'dev-util/gtk-doc'          : 'platform:gtk-doc',
        'dev-libs/libIDL'           : 'platform:libIDL',
        'media-libs/libart_lgpl'    : 'platform:libart_lgpl',
        'gnome-base/libbonobo'      : 'platform:libbonobo',
        'gnome-base/libbonoboui'    : 'platform:libbonoboui',
        'gnome-base/libglade'       : 'platform:libglade',
        'gnome-base/libgnome'       : 'platform:libgnome',
        'gnome-base/libgnomecanvas' : 'platform:libgnomecanvas',
        'gnome-base/libgnomeui'     : 'platform:libgnomeui',
        'x11-libs/pango'            : 'platform:pango',
        # missing: platform:gnome-vfs-monikers

        ## Desktop
        'x11-misc/alacarte'         : 'desktop:alacarte',
        'app-cdr/brasero'           : 'desktop:brasero',
        'gnome-extra/bug-buddy'     : 'desktop:bug-buddy',
        'media-video/cheese'        : 'desktop:cheese',
        'app-accessibility/dasher'  : 'desktop:dasher',
        'gnome-extra/deskbar-applet': 'desktop:deskbar-applet',
        'net-voip/ekiga'            : 'desktop:ekiga',
        'net-im/empathy'            : 'desktop:empathy',
        'media-gfx/eog'             : 'desktop:eog',
        'www-client/epiphany'       : 'desktop:epiphany',
        'app-text/evince'           : 'desktop:evince',
        'mail-client/evolution'     : 'desktop:evolution',
        'gnome-extra/evolution-data-server' : 'desktop:evolution-data-server',
        'gnome-extra/evolution-exchange'    : 'desktop:evolution-exchange',
        'gnome-extra/evolution-webcal'      : 'desktop:evolution-webcal',
        'app-arch/file-roller'      : 'desktop:file-roller',
        'gnome-extra/gcalctool'     : 'desktop:gcalctool',
        'gnome-extra/gconf-editor'  : 'desktop:gconf-editor',
        'gnome-base/gdm'            : 'desktop:gdm',
        'app-editors/gedit'         : 'desktop:gedit',
        'gnome-base/gnome-applets'  : 'desktop:gnome-applets',
        'x11-themes/gnome-backgrounds': 'desktop:gnome-backgrounds',
        'net-wireless/gnome-bluetooth': 'desktop:gnome-bluetooth',
        'gnome-base/gnome-control-center': 'desktop:gnome-control-center',
        'dev-dotnet/gnome-desktop-sharp' : 'desktop:gnome-desktop-sharp',
        'sys-apps/gnome-disk-utility' : 'desktop:gnome-disk-utility',
        'app-text/gnome-doc-utils' : 'desktop:gnome-doc-utils',
        'gnome-extra/gnome-games' : 'desktop:gnome-games',
        'x11-themes/gnome-icon-theme' : 'desktop:gnome-icon-theme',
        'gnome-base/gnome-keyring' : 'desktop:gnome-keyring',
        'app-accessibility/gnome-mag' : 'desktop:gnome-mag',
        'gnome-extra/gnome-media'   : 'desktop:gnome-media',
        'gnome-base/gnome-menus'    : 'desktop:gnome-menus',
        'net-analyzer/gnome-netstatus' : 'desktop:gnome-netstatus',
        'net-analyzer/gnome-nettool' : 'desktop:gnome-nettool',
        'gnome-extra/gnome-packagekit' : 'desktop:gnome-packagekit',
        'gnome-base/gnome-panel'    : 'desktop:gnome-panel',
        'gnome-extra/gnome-power-manager' : 'desktop:gnome-power-manager',
        'dev-python/gnome-python-desktop' : 'desktop:gnome-python-desktop',
        'gnome-extra/gnome-screensaver' : 'desktop:gnome-screensaver',
        'gnome-base/gnome-session'  : 'desktop:gnome-session',
        'gnome-base/gnome-settings-daemon' : 'desktop:gnome-settings-daemon',
        'dev-dotnet/gnome-sharp' : 'desktop:gnome-sharp',
        'app-accessibility/gnome-speech' : 'desktop:gnome-speech',
        'gnome-extra/gnome-system-monitor' : 'desktop:gnome-system-monitor',
        'app-admin/gnome-system-tools' : 'desktop:gnome-system-tools',
        'x11-terms/gnome-terminal' : 'desktop:gnome-terminal',
        'x11-themes/gnome-themes' : 'desktop:gnome-themes',
        'gnome-extra/gnome-user-docs' : 'desktop:gnome-user-docs',
        'gnome-extra/gnome-user-share' : 'desktop:gnome-user-share',
        'gnome-extra/gnome-utils' : 'desktop:gnome-utils',
        'app-accessibility/gok' : 'desktop:gok',
        'media-libs/gst-plugins-base' : 'desktop:gst-plugins-base',
        'media-libs/gst-plugins-good' : 'desktop:gst-plugins-good',
        'media-libs/gstreamer' : 'desktop:gstreamer',
        'x11-themes/gtk-engines' : 'desktop:gtk-engines',
        'gnome-extra/gtkhtml' : 'desktop:gtkhtml',
        'x11-libs/gtksourceview' : 'desktop:gtksourceview',
        'gnome-extra/gucharmap' : 'desktop:gucharmap',
        'gnome-base/gvfs' : 'desktop:gvfs',
        'gnome-extra/hamster-applet' : 'desktop:hamster-applet',
        'gnome-extra/libgail-gnome' : 'desktop:libgail-gnome',
        'gnome-base/libgnome-keyring' : 'desktop:libgnome-keyring',
        'gnome-base/libgnomekbd' : 'desktop:libgnomekbd',
        'gnome-base/libgnomeprint' : 'desktop:libgnomeprint',
        'gnome-base/libgnomeprintui' : 'desktop:libgnomeprintui',
        'gnome-base/libgtop' : 'desktop:libgtop',
        'dev-libs/libgweather' : 'desktop:libgweather',
        'dev-libs/liboobs' : 'desktop:liboobs',
        'gnome-base/librsvg' : 'desktop:librsvg',
        'net-libs/libsoup' : 'desktop:libsoup',
        'x11-libs/libwnck' : 'desktop:libwnck',
        'x11-wm/metacity' : 'desktop:metacity',
        'gnome-base/nautilus' : 'desktop:nautilus',
        'gnome-extra/nautilus-sendto' : 'desktop:nautilus-sendto',
        'app-accessibility/orca' : 'desktop:orca',
        'dev-python/pygtksourceview' : 'desktop:pygtksourceview',
        'app-crypt/seahorse' : 'desktop:seahorse',
        'app-crypt/seahorse-plugins' : 'desktop:seahorse-plugins',
        'media-sound/sound-juicer' : 'desktop:sound-juicer',
        'gnome-extra/swfdec-gnome' : 'desktop:swfdec-gnome',
        'app-misc/tomboy' : 'desktop:tomboy',
        'media-video/totem' : 'desktop:totem',
        'dev-libs/totem-pl-parser' : 'desktop:totem-pl-parser',
        'net-misc/vinagre' : 'desktop:vinagre',
        'net-misc/vino' : 'desktop:vino',
        'x11-libs/vte' : 'desktop:vte',
        'gnome-extra/yelp' : 'desktop:yelp',
        'gnome-extra/zenity' : 'desktop:zenity',
        # missing: desktop:at-spi2-atk
        # missing: desktop:at-spi2-core
        # missing: desktop:evolution-mapi

        ## Admin
        'app-admin/pessulus' : 'admin:pessulus',
        'app-admin/sabayon' : 'admin:sabayon',

        ## Devtools
        'app-accessibility/accerciser' : 'devtools:accerciser',
        'dev-util/anjuta' : 'devtools:anjuta',
        'dev-util/devhelp' : 'devtools:devhelp',
        'dev-libs/gdl' : 'devtools:gdl',
        'dev-util/glade' : 'devtools:glade3',
        'dev-util/gnome-devel-docs' : 'devtools:gnome-devel-docs'
        # TODO: bindings
    }

    # Url for fetching version information
    url = 'http://ftp.gnome.org/pub/GNOME'
    #teams/releng/2.30.2/versions'
    ids = ('platform', 'desktop', 'admin', 'devtools', 'bindings')

    def __init__(self):
        pass

    def fetch_and_parse_all(self):
        """Download and parse package version information."""
        items = {}
        # Read the latest release tarball versions using SHA256SUMS-files
        for x in self.ids:
            # FIXME: GNOME version is hardcoded (needs extra options, somehow...)
            data = urllib2.urlopen('%s/%s/2.30/2.30.2/sources/SHA256SUMS-for-bz2' % \
                        (self.url, x))
            for line in data.readlines():
                # Extract tarball names with versions
                rawline = line.strip().split('  ')[1][:-len('.tar.bz2')]
                pkg, ver = rawline.rsplit('-', 1)
                items["%s:%s" % (x, pkg)] = [ver]
            data.close()
        return items

if __name__ == '__main__':
    print gnome_checker().fetch_and_parse_all()

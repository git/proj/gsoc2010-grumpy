# -*- coding: utf-8 -*-
"""
    grumpy.testsuite.pkgmodel
    ~~~~~~~~~~~~~~~~~~~~~~~~~

    Unittests for package and ebuild handling.

    :copyright: (c) by 2010 Priit Laes.
    :license: BSD, see LICENSE for details.
"""
from . import GrumpyTestCase

from pkgcore.ebuild.repo_objs import Maintainer
from grumpy.models import Category, Developer, Ebuild, Herd, Package

import unittest

class PkgModelTestCase(GrumpyTestCase):

    def test_portage_data_model(self):
        ## Create portage data
        # app-misc/testpkg
        C1 = 'app-misc'
        data = {'EAPI': '0', 'SLOT': '1.1', \
                'DESCRIPTION': u'this is description', \
                'HOMEPAGE': 'http://example.org/testpkg-app-misc', \
                'KEYWORDS':'x86 amd64', \
                'IUSE':'test +positive flags -negative split-flag'}
        sdata = {'longdescription': 'this is longdescription', \
                 'herds': (None, '', 'kala', 'test'), \
                 'maintainers': (Maintainer('test@gentoo.org'), Maintainer('nobody@gentoo.org'))
                 }
        p1 = self.get_pkg('%s/testpkg-1.0-r3' % C1, sdata, data)
        data['KEYWORDS'] = 'x86 ~amd64'
        p2 = self.get_pkg('%s/testpkg-1.0-r4' % C1, sdata, data)

        # app-test/testpkg
        C2 = 'app-test'
        data = {'EAPI': '0', 'SLOT': '1.0', \
                'DESCRIPTION': 'short description', \
                'HOMEPAGE': 'http://example.org/testpkg-app-test', \
                'KEYWORDS':'x86 ~amd64', \
                'IUSE':'test +positive flags -negative split-flag'}
        p3 = self.get_pkg('%s/testpkg-1.0-r3' % C2, sdata, data)

        ## Create and test categories
        c = [Category(C1), Category(C2)]
        self.db.session.add_all(c)
        self.db.session.commit()
        assert Category.query.count() == 2

        ## Create and test packages
        c[0].packages[p1.key] = Package(p1)
        self.db.session.commit()
        c[1].packages[p3.key] = Package(p3)
        self.db.session.commit()
        assert Package.query.count() == 2
        # TODO: assert p.category vs package.category.name

        p = Package.query.filter_by(key='%s/testpkg' % C1).first()
        assert p.pkg == 'testpkg'
        assert p.desc == 'this is description'
        assert p.ldesc == 'this is longdescription'
        assert p.homepage == 'http://example.org/testpkg-app-misc'
        assert p.category.name == C1
        assert len(p.category.packages) == 1
        assert len(p.herds) == 3
        assert len(p.devs) == 2

        p = Package.query.filter_by(key='%s/testpkg' % C2).first()
        assert p.pkg == 'testpkg'
        assert p.desc == 'short description'
        assert p.ldesc == 'this is longdescription'
        assert p.homepage == 'http://example.org/testpkg-app-test'
        assert p.category.name == C2
        assert len(p.category.packages) == 1
        assert len(p.herds) == 3
        assert len(p.devs) == 2

        # Test changes in herds
        sdata['herds'] = ()
        px = self.get_pkg('%s/testpkg-1.0-r3' % C1, sdata, data)
        c[0].packages[px.key].sync(px)

        self.db.session.commit()
        p = Package.query.filter_by(key='%s/testpkg' % C1).first()
        assert p.pkg == 'testpkg'
        assert p.category.name == C1
        assert len(p.herds) == 0

        # Test Category -> package lookup
        for p in (p1, p2, p3):
            c = Category.query.filter_by(name=p.category).first()
            assert c != None
            assert c.packages[p.key].key == p.key

        # Handle ebuilds
        c = Category.query.filter_by(name=p1.category).first()
        pkg = c.packages[p1.key]
        pkg.ebuilds[p1.cpvstr] = Ebuild(p1)
        pkg.ebuilds[p2.cpvstr] = Ebuild(p2)
        self.db.session.commit()
        assert Ebuild.query.count() == 2

        e = pkg.ebuilds[p1.cpvstr]
        assert e.cpv == p1.cpvstr
        assert e.iuse == 'test,flags,split-flag'
        assert e.iuse_neg == 'negative'
        assert e.iuse_pos == 'positive'
        assert e.keywords == 'x86,amd64'
        assert e.eapi == 0
        assert e.slot == '1.1'
        assert e.fullver == p1.fullver
        assert e.package == pkg

        e = pkg.ebuilds[p2.cpvstr]
        assert e.cpv == p2.cpvstr
        assert e.iuse == 'test,flags,split-flag'
        assert e.iuse_neg == 'negative'
        assert e.iuse_pos == 'positive'
        assert e.keywords == 'x86,~amd64'
        assert e.eapi == 0
        assert e.slot == '1.1'
        assert e.fullver == p2.fullver
        assert e.package == pkg

        # Add more ebuilds to tree
        c = Category.query.filter_by(name=p3.category).first()
        pkg = c.packages[p3.key]
        pkg.ebuilds[p1.cpvstr] = Ebuild(p3)
        self.db.session.commit()

        assert Ebuild.query.count() == 3
        assert Package.query.count() == 2
        assert Category.query.count() == 2
        assert Herd.query.count() == 3
        assert Developer.query.count() == 2

        # Delete second category
        c = Category.query.filter_by(name=p3.category).first()
        self.db.session.delete(c)
        self.db.session.commit()
        assert Category.query.count() == 1
        assert Package.query.count() == 1
        assert Ebuild.query.count() == 2
        # Delete first category
        c = Category.query.filter_by(name=p1.category).first()
        self.db.session.delete(c)
        self.db.session.commit()
        assert Category.query.count() == 0
        assert Package.query.count() == 0
        assert Ebuild.query.count() == 0

        # Herds and maintainers should be still in db
        assert Herd.query.count() == 3
        assert Developer.query.count() == 2

    def test_package_rename(self):
        self.make_package(self.get_pkg('sys-apps/action-1.0-r2', {}, {}))
        assert Category.query.count() == 1
        assert Package.query.count() == 1
        assert Ebuild.query.count() == 1

        p = Package.query.filter_by(key='sys-apps/action').first()
        assert p != None
        p.rename('sys-fail/kala')
        self.db.session.commit()
        assert Category.query.count() == 2
        assert Package.query.count() == 1
        assert Ebuild.query.count() == 1
        p = Package.query.filter_by(key='sys-apps/action').first()
        assert p == None
        p = Package.query.filter_by(key='sys-fail/kala').first()
        assert p != None
        assert p.pkg == 'kala'
        e = Ebuild.query.first()
        assert e.cpv == 'sys-fail/kala-1.0-r2'

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(PkgModelTestCase))
    return suite

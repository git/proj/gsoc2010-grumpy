# -*- coding: utf-8 -*-
"""
    grumpy.testsuite
    ~~~~~~~~~~~~~~~~

    Unittests for Grumpy project.

    :copyright: (c) by 2010 Priit Laes.
    :license: BSD, see LICENSE for details.
"""
import copy, unittest

from pkgcore.ebuild import ebuild_src, repo_objs

from grumpy import app
from grumpy.models import db, Category, Ebuild, Package

class GrumpyTestCase(unittest.TestCase):

    def make_package(self, package):
        cat = package.key.split('/')[0]
        c = Category.query.filter_by(name=cat).first()
        if not c:
            c = Category(cat)
            self.db.session.add(c)
        p = Package(package)
        c.packages[package.key] = p
        p.ebuilds[package.cpvstr] = Ebuild(package)
        self.db.session.commit()

    def get_pkg(self, cpv, pkg_data={}, data={}):
        """Returns a custom ebuild"""
        # We need to set up info from metadata.xml separately
        valid_keys = ("longdescription", "maintainers", "herds")
        for x in valid_keys:
            pkg_data.setdefault(x, "")
        metadata = repo_objs.MetadataXml(None)
        for key, value in copy.copy(pkg_data).iteritems():
            if key not in valid_keys:
                continue
            object.__setattr__(metadata, "_" + key, value)
        shared_pkg_data = repo_objs.SharedPkgData(metadata, None)

        o = ebuild_src.package(shared_pkg_data, None, cpv)
        if data is not None:
            object.__setattr__(o, 'data', copy.copy(data))
        return o

    def setUp(self):
        #app.config['SQLALCHEMY_ECHO'] = True
        app.config['SQLALCHEMY_ENGINE'] = 'sqlite://'
        #app.config['SQLALCHEMY_ENGINE'] = 'postgresql+psycopg://grumpy:grumpy@localhost/grumpy_test'
        app.config['TESTING'] = True
        db.create_all()

        # Initialize request context
        app.test_request_context().push()

        self.app = app
        self.db = db

    def tearDown(self):
        self.db.session.rollback()
        self.db.drop_all()

def suite():
    from . import favorites, pkgmodel, usermodel
    suite = unittest.TestSuite()
    suite.addTest(favorites.suite())
    suite.addTest(pkgmodel.suite())
    suite.addTest(usermodel.suite())
    return suite

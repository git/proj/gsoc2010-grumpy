# -*- coding: utf-8 -*-
"""
    grumpy.testsuite.favorites
    ~~~~~~~~~~~~~~~~~~~~~~~~~~

    Unittests for "favorite" user packages.

    :copyright: (c) by 2010 Priit Laes.
    :license: BSD, see LICENSE for details.
"""
import unittest

from . import GrumpyTestCase
from grumpy.models import Package, User

class FavoritesTestCase(GrumpyTestCase):

    d1 = {'DESCRIPTION': u'Desc',
          'HOMEPAGE': 'http://example.com',
          'KEYWORDS': '~amd64'}
    d2 = {'longdescription': 'longdesc',
          'herds': ('test'),
          'maintainers': ()}
    ps = ['sys-test/kala-0.12', 'sys-kala/test-0.10', 'sys-apps/ffff-0.1', \
          'sys-apps/fffuuu-0.9', 'sys-apps/ssss-0.3', 'kala-base/appp-3.0']

    def make_users(self):
        u = [User('user1@gentoo.org', 'http://example.net/openid1'),
             User('user2@gentoo.org', 'http://example.net/openid2')]
        self.db.session.add_all(u)
        self.db.session.commit()
        return u

    def test_favorites(self):
        u = self.make_users()
        assert User.query.count() == 2
        for i in self.ps:
            self.make_package(self.get_pkg(i, self.d2, self.d1))
        assert Package.query.count() == len(self.ps)

        # Fetch some packages
        pkgs = Package.query.filter(Package.key.in_(\
               ['sys-test/kala', 'kala-base/appp', 'sys-apps/ssss'])).all()
        for p in pkgs:
            u[0].favorites.append(p)
        pkgs = Package.query.filter(Package.key.in_(\
               ['sys-apps/fffuuu', 'kala-base/appp'])).all()
        for p in pkgs:
            u[1].favorites.append(p)
        self.db.session.commit()

        assert len(u[0].favorites) == 3
        assert len(u[1].favorites) == 2
        p = Package.query.filter_by(key='sys-apps/ssss').one()
        assert len(p.favorites) == 1
        p = Package.query.filter_by(key='kala-base/appp').one()
        assert len(p.favorites) == 2


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(FavoritesTestCase))
    return suite

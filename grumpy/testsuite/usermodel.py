# -*- coding: utf-8 -*-
"""
    grumpy.testsuite.usermodel
    ~~~~~~~~~~~~~~~~~~~~~~~~~~

    Various user handling unittests.

    :copyright: (c) by 2010 Priit Laes.
    :license: BSD, see LICENSE for details.
"""
from . import GrumpyTestCase

from grumpy.models import User

import unittest

class UserModelTestCase(GrumpyTestCase):

    def make_users(self):
        self.db.session.add_all([\
            User('user1@gentoo.org', 'http://example.net/openid1'),
            User('user2@gentoo.org', 'http://example.net/openid2')])
        self.db.session.commit()

    def test_user_creation(self):
        self.make_users()
        assert User.query.count() == 2

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(UserModelTestCase))
    return suite

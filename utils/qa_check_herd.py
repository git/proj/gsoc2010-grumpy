#! /usr/bin/env python
import os, sys
import urllib2

from datetime import datetime
from lxml.etree import fromstring
from optparse import OptionParser

path = os.path.join(os.path.dirname(__file__), os.path.pardir)
sys.path.insert(0, path)
del path

from grumpy import app
from grumpy.models import db, Herd, PkgIssue, Setting

HERDS_URL = 'http://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo/xml/htdocs/proj/en/metastructure/herds/herds.xml'
PLUGIN_NAME='qa::valid_herd'

def gc_collect(timestamp):
    """Remove old QA issues from database returning number of rows deleted."""
    db.session.expire_all()
    print ("DEBUG: Deleted %d resolved (?) issues." % PkgIssue.query \
                        .filter_by(plugin=PLUGIN_NAME) \
                        .filter(PkgIssue.created_on < timestamp).delete(False))

def update_issues(invalid):
    """Insert QA issues into db."""
    cases = {'maintainer-needed': 'Package is looking for maintainer-herd',
             'no-herd': 'Package is missing herd',
             'fix-me' : 'Package should use "no-herd" instead of empty "herd" tag',
             'invalid-herd' : 'Herd %s is not listed in official herd list'
    }
    for herd in invalid:
        issue = herd
        if herd not in cases.keys():
            issue = 'invalid-herd'
            data = cases[issue] % herd
        else:
            data = cases[herd]
        try:
            # Clean up old issues
            PkgIssue.query.filter_by(plugin=PLUGIN_NAME,type=herd).delete(False)
            for pkg in Herd.query.filter_by(name=herd).first().packages:
                pkg.qaissues.append(PkgIssue(pkg, PLUGIN_NAME, issue, data))
            db.session.commit()
        except Exception:
            db.session.rollback()
            print "Invalid error occurred!"
            raise RuntimeError

def download_and_parse_herds():
    """Return list of herd names from 'herds.xml'"""
    data = urllib2.urlopen(HERDS_URL)
    herds = []
    for child in fromstring(data.read()).getchildren():
        for value in child.getchildren():
            if value.tag == 'name':
                herds.append(value.text)
    return herds

if __name__ == '__main__':
    parser = OptionParser(usage="usage: %prog [options] CONFFILE")
    (opts, args) = parser.parse_args()
    if len(args) != 1:
        parser.error("provide path to configuration file as first argument")
        sys.exit(1)
    # Fetch data and parse it
    herds = download_and_parse_herds()
    b0rks = []
    timestamp = datetime.now()
    app.test_request_context().push()
    # Load configuration
    app.config.from_pyfile(args[0])
    # Fetch list of herds from db
    for herd in Herd.query.all():
        if herd.name in herds:
            herds.remove(herd.name)
        else:
            b0rks.append(herd.name)
    update_issues(b0rks)
    # Clean up issues < timestamp
    gc_collect(timestamp)
    # Update settings and add info about last run..
    Setting.query.filter_by(name=PLUGIN_NAME).delete(False)
    db.session.add(Setting(PLUGIN_NAME, str(timestamp)))
    db.session.commit()

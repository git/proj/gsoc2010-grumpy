#! /usr/bin/env python
import os, sys
from datetime import datetime
from optparse import OptionParser
from werkzeug.utils import find_modules, import_string

path = os.path.join(os.path.dirname(__file__), os.path.pardir)
sys.path.insert(0, path)
del path

from grumpy import app
from grumpy.models import db, Package, PkgIssue, Setting
from grumpy.utils import compare_version

PLUGIN_NAME='ver_bumper::'
def print_list(mods):
    return "Supported modules are:\n\t%s" % ('\n\t'.join(mods))

if __name__ == '__main__':
    # Look up supported modules
    modules = [i.rsplit('.', 1)[1] for i in find_modules('grumpy.vdb')]
    parser = OptionParser(usage="usage: %prog [options] CONFFILE module")
    (opts, args) = parser.parse_args()
    if len(args) != 2:
        parser.error("provide path to configuration file as first argument\n" \
                     "package module you want to load as second argument.\n\n" \
                     "%s" % print_list(modules))
        sys.exit(1)
    # Check whether module is supported
    mod = args[1]
    if mod not in modules:
        parser.error("Unknown module: %s\n%s" % (mod, print_list(modules)))
        sys.exit(1)
    # Import our version checker module
    m = import_string('grumpy.vdb.%s.%s_checker' % (mod, mod), True)
    if not m:
        print "Unknown error occurred: unable to import module %s" % mod
        raise RuntimeError
    module = m()
    items = module.fetch_and_parse_all()
    # Gather package versions
    updates = {}
    missing = []
    for p in module.pkgs:
        pyp = module.pkgs[p]
        if pyp not in items.keys():
            missing.append([p, pyp])
        else:
            updates[p] = items[pyp]
    PLUGIN_NAME = PLUGIN_NAME + module.__class__.__name__
    with app.test_request_context():
        timestamp = datetime.now()
        app.config.from_pyfile(args[0])
        try:
            # Delete existing issues
            print ("DEBUG: Deleted %d old issues." % PkgIssue.query \
                        .filter_by(plugin=PLUGIN_NAME) \
                        .filter(PkgIssue.created_on < timestamp).delete(False))
            db.session.commit()

            # File 'missing' package mapping issues
            for item in missing:
                p = Package.query.filter_by(key=item).first()
                if not p:
                    print "DEBUG: package '%s' is missing from upstream." % item
                    continue
                p.qaissues.append(PkgIssue(p, PLUGIN_NAME, \
                    'missing-upstream', \
                    'No package "%s" found in upstream: %s' % \
                        (item, module.__class__.__name__)))
            # Run version checks
            # TODO: Figure out slotted ebuilds
            for pkg, ver in updates.iteritems():
                p = Package.query.filter_by(key=pkg).first()
                if not p:
                    print "DEBUG: package '%s' is missing from portage." % item
                    continue
                # We need to sort pkg versions
                ver = sorted(ver, compare_version, reverse=True)
                res = compare_version(ver[0], p.versions[0])
                if res == -1:
                    p.qaissues.append(PkgIssue(p, PLUGIN_NAME, \
                    'version-mismatch', 'Package "%s" has newer version in ' \
                    'Portage than on "%s" upstream' % \
                    (pkg, module.__class__.__name__)))
                elif res == 1:
                    p.qaissues.append(PkgIssue(p, PLUGIN_NAME, \
                    'version-bump', 'Package "%s" has newer version "%s" '
                    'available in "%s" upstream ' % \
                    (pkg, p.versions[0], module.__class__.__name__)))
                db.session.commit()
            # Update settings and add info about last run..
            Setting.query.filter_by(name=PLUGIN_NAME).delete(False)
            db.session.add(Setting(PLUGIN_NAME, str(timestamp)))
            db.session.commit()
        except Exception:
            db.session.rollback()

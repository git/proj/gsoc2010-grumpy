#! /usr/bin/env python
import os, sys

from optparse import OptionParser

path = os.path.join(os.path.dirname(__file__), os.path.pardir)
sys.path.insert(0, path)
del path

from grumpy import app
from grumpy.models import db

if __name__ == '__main__':
    parser = OptionParser(usage="usage: %prog [options] CONFFILE")
    (opts, args) = parser.parse_args()
    if len(args) != 1:
        parser.error("provide path to configuration file as first argument")
        sys.exit(1)
    app.test_request_context().push()
    app.config.from_pyfile(args[0])
    db.create_all()

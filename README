==============
Project Grumpy
==============

Project Grumpy is a maintainer-oriented set of tools for handling various
developer-related metadata.

Requirements
============

  - Python-2.5+
  - pkgcore
  - SQLAlchemy-0.6
  - Flask
  - Flask-OpenID
  - Flask-SQLAlchemy

Database support
----------------

Although main development is aimed towards use of PostgreSQL, most of the
databases supported by SQLAlchemy should work fine.

Database configuration is defined by specifying the RFC-1738 style uri::

  dialect+driver://username:password@host:port/database

Installation
============

Currently the installation of Grumpy is possible on POSIX like systems (BSD
or Linux).

When deploying on Apache, open Apache or vhost config and add the following
lines::

  WSGIScriptAlias /grumpy /path/to/grumpy/grumpy.wsgi

This tells Apache that Grumpy application is hooked on your webserver at
location `/grumpy`. Of course you will need mod_wsgi and need to reload Apache
in order to pick your changes up.

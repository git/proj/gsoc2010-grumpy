"""
Project Grumpy
--------------

Project Grumpy is a an application and a set of utilities aimed to improve
life for Gentoo developers.

"""
import os, sys

from os.path import join
from setuptools import Command, setup

class cmd_Audit(Command):
    """Audits Grumpy's source code using PyFlakes for following issues:
        - Names which are used but not defined or used before they are defined.
        - Names which are redefined without having been used.
    """

    description = "Audit Grumpy's source with PyFlakes"
    user_options = []

    def initialize_options(self):
        all = None

    def finalize_options(self):
        pass

    def run(self):
        try:
            import pyflakes.scripts.pyflakes as flakes
        except ImportError:
            print "Audit requires PyFlakes installed in your system."""
            sys.exit(-1)

        dirs = ['grumpy', 'grumpy/testsuite', 'grumpy/vdb', 'utils']
        warns = 0
        for dir in dirs:
            for filename in os.listdir(dir):
                if filename.endswith('.py') and filename != '__init__.py':
                    warns += flakes.checkPath(os.path.join(dir, filename))
        if warns > 0:
            print ("Audit finished with total %d warnings." % warns)
        else:
            print ("No problems found in sourcecode.")

setup(
    name='Grumpy',
    version='0.0',
    url='http://git.overlays.gentoo.org/gitweb/?p=proj/grumpy.git;a=summary',
    license='BSD',
    author='Priit Laes',
    author_email='plaes@plaes.org',
    description='Set of QA helpers for Gentoo Developers',
    long_description=__doc__,
    packages=['grumpy'],
    scripts=[join('utils', 'grumpy_sync.py')],
    test_suite='grumpy.testsuite.suite',
    zip_safe=False,
    platforms='any',
    install_requires=[
        'Flask',
        'Flask-OpenID',
        'Flask-SQLAlchemy',
        'lxml',
        'pkgcore',
        'snakeoil',
        'SQLAlchemy >= 0.6',
    ],
    classifiers=[
        'Development Status :: 1 - Planning',
        'Environment :: Other Environment',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Quality Assurance',
        'Topic :: Utilities'
    ],
    cmdclass={'audit': cmd_Audit}
)

